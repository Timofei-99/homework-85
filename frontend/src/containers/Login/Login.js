import React, {useState} from 'react';
import {Avatar, Button, Container, Grid, Link, makeStyles, Typography} from "@material-ui/core";
import LockOpenOutlinedIcon from "@material-ui/icons/LockOpenOutlined";
import FormElement from "../../components/UI/Form/FormElement";
import {Link as RouterLink} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {loginUser} from "../../store/actions/userActions";
import {Alert} from "@material-ui/lab";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";
import FacebookLogin from "../../components/UI/FacebookLogin/FacebookLogin";


const useStyles = makeStyles(theme => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    alert: {
        marginTop: theme.spacing(3),
        maxWidth: '100%',
    },
}));

const Login = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const error = useSelector(state => state.users.loginError);
    const loading = useSelector(state => state.users.LoginLoading)

    const [user, setUser] = useState({
        email: '',
        password: ''
    });

    const inputChangeHandler = e => {
        const {name, value} = e.target;
        setUser(prevState => ({...prevState, [name]: value}));
    };

    const submitFormHandler = e => {
        e.preventDefault();
        dispatch(loginUser({...user}))
    };

    return (
        <Container component='section' maxWidth='xs'>
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOpenOutlinedIcon/>
                </Avatar>
                <Typography component='h1' variant='h6'>
                    Sign in
                </Typography>
                {error &&
                <Alert severity='error' className={classes.alert}>
                    {error.message || error.global}
                </Alert>}
                <Grid
                    component='form'
                    container
                    className={classes.form}
                    spacing={2}
                    onSubmit={submitFormHandler}
                    noValidate
                >

                    <FormElement
                        required
                        type='text'
                        autoComplete='new-username'
                        value={user.email}
                        label='Email'
                        onChange={inputChangeHandler}
                        name='email'
                    />

                    <FormElement
                        required
                        type='password'
                        autoComplete='new-password'
                        value={user.password}
                        label='Password'
                        onChange={inputChangeHandler}
                        name='password'
                    />

                    <Grid item xs={12}>
                        <ButtonWithProgress
                            type='submit'
                            fullWidth
                            variant='contained'
                            color='primary'
                            className={classes.submit}
                            loading={loading}
                            disabled={loading}
                        >
                            Sign in
                        </ButtonWithProgress>

                        <Grid item>
                            <FacebookLogin/>
                        </Grid>

                    </Grid>



                    <Grid item container justifyContent='flex-end'>
                        <Link component={RouterLink} variant='body2' to='/register'>
                            Or sign up
                        </Link>
                    </Grid>
                </Grid>
            </div>
        </Container>
    );
};

export default Login;