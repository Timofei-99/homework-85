import React, {useEffect} from 'react';
import {Grid, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {fetchArtist} from "../store/actions/ArtistAction";
import Artist from "../components/Artist";
import BackDrop from "../components/UI/BackDrop/BackDrop";

const ArtistList = () => {
    const dispatch = useDispatch();
    const artists = useSelector(state => state.artists.artists);
    const loading = useSelector(state => state.artists.loading);
    const user = useSelector(state => state.users.user);

    useEffect(() => {
        dispatch(fetchArtist())
    }, [dispatch]);

    return (
        <Grid container direction={'column'}>
            <BackDrop
                loading={loading}
            />
            <Grid item container justifyContent='space-between' alignItems='center'>
                <Grid item>
                    <Typography variant='h4' style={{marginBottom: '20px'}}>Artist List</Typography>
                </Grid>
            </Grid>
            <Grid item container spacing={3}>
                {artists.map(artist => (
                    artist?.published === true || user?.role === 'admin' ?
                        <Artist
                            key={artist._id}
                            title={artist.name}
                            image={artist.image}
                            _id={artist._id}
                            artist={artist}
                        /> : null
                ))}
            </Grid>
        </Grid>
    );
};

export default ArtistList;