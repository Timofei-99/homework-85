import React, {useEffect} from 'react';
import {Grid, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {fetchTracks} from "../store/actions/TrackAction";
import Track from "../components/Track";
import BackDrop from "../components/UI/BackDrop/BackDrop";

const Tracks = ({location}) => {
    const dispatch = useDispatch();
    const tracks = useSelector(state => state.tracks.tracks);
    const loading = useSelector(state => state.tracks.loading);

    useEffect(() => {
        dispatch(fetchTracks(location.search))
    }, [dispatch]);

    return (
        <Grid container direction={'column'}>
            <BackDrop
                loading={loading}
            />
            <Grid item>
                <Grid item>
                    <Typography style={{marginBottom: '20px'}} variant={'h4'}>Tracks</Typography>
                </Grid>
            </Grid>
            <Grid><Typography style={{marginBottom: '20px'}} variant={'h4'}>{tracks[0]?.album.artist.name}</Typography></Grid>
            <Grid item>
                {tracks.length > 0 ? tracks.map(tr => (
                    <Track
                        _id={tr._id}
                        key={tr._id}
                        track={tr.name}
                        album={tr.album.name}
                        duration={tr.duration}
                        number={tr.number}
                        publish={tr}
                        location={location.search}
                    />
                )) : (<Typography variant='h4' style={{alignItems: 'center'}}>Список треков пуст</Typography>)}
            </Grid>
        </Grid>
    );
};

export default Tracks;