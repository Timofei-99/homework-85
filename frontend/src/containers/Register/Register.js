import React, {useState} from 'react';
import {Link as RouterLink} from 'react-router-dom';

import {Avatar, Button, Container, Grid, Link, makeStyles, Typography} from "@material-ui/core";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import {useDispatch, useSelector} from "react-redux";
import {registerUser} from "../../store/actions/userActions";
import FormElement from "../../components/UI/Form/FormElement";
import FileInput from "../../components/UI/Form/FileInput/FileInput";

const useStyles = makeStyles(theme => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));


const Register = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const error = useSelector(state => state.users.registerError);

    const [user, setUser] = useState({
        email: '',
        password: '',
        image: '',
        displayName: ''
    });

    const inputChangeHandler = e => {
        const {name, value} = e.target;
        setUser(prevState => ({...prevState, [name]: value}));
    };

    const submitFormHandler = e => {
            e.preventDefault();

            const formData = new FormData();

            Object.keys(user).forEach((key) => {
                formData.append(key, user[key]);
            });

            dispatch(registerUser(formData));
    };

    const fileChangeHandler = (e) => {
        const name = e.target.name;
        const file = e.target.files[0];

        setUser((prevState) => ({
            ...prevState,
            [name]: file,
        }));
    };

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    return (
        <Container component='section' maxWidth='xs'>
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon/>
                </Avatar>
                <Typography component='h1' variant='h6'>
                    Sign up
                </Typography>
                <Grid
                    component='form'
                    container
                    className={classes.form}
                    spacing={2}
                    onSubmit={submitFormHandler}
                >

                    <FormElement
                        type='text'
                        autoComplete='new-username'
                        value={user.email}
                        label='Email'
                        onChange={inputChangeHandler}
                        name='email'
                        error={getFieldError('email')}
                    />

                    <FormElement
                        type='password'
                        autoComplete='new-password'
                        value={user.password}
                        label='Password'
                        onChange={inputChangeHandler}
                        name='password'
                        error={getFieldError('password')}
                    />

                    <FormElement
                        type='text'
                        label='Display Name'
                        value={user.displayName}
                        onChange={inputChangeHandler}
                        name={'displayName'}
                        error={getFieldError('displayName')}
                    />


                    <Grid item xs={12}>
                        <Grid item xs>
                            <FileInput onChange={fileChangeHandler} label='Avatar image' name='image'/>
                        </Grid>
                        <Button
                            type='submit'
                            fullWidth
                            variant='contained'
                            color='primary'
                            className={classes.submit}
                        >
                            Sign up
                        </Button>
                    </Grid>

                    <Grid item container justifyContent='flex-end'>
                        <Link component={RouterLink} variant='body2' to='/login'>
                            Already have an account? Sign in
                        </Link>
                    </Grid>
                </Grid>
            </div>
        </Container>
    );
};

export default Register;