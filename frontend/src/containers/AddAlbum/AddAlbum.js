import React, {useEffect, useState} from 'react';
import {Button, Grid, MenuItem, TextField, Typography} from "@material-ui/core";
import FormElement from "../../components/UI/Form/FormElement";
import {useDispatch, useSelector} from "react-redux";
import {fetchArtist} from "../../store/actions/ArtistAction";
import FileInput from "../../components/UI/Form/FileInput/FileInput";
import {postAlbum} from "../../store/actions/AlbumAction";

const AddAlbum = () => {
    const dispatch = useDispatch();

    const [artist, setArtist] = useState({
        name: '',
        artist: '',
        year: '',
        image: '',
    });

    useEffect(() => {
        dispatch(fetchArtist());
    }, [dispatch]);

    const artists = useSelector(state => state.artists.artists);
    console.log(artists)


    const inputChangeHandler = e => {
        const {name, value} = e.target;

        setArtist(prevState => ({
            ...prevState,
            [name]: value
        }));
    };


    const fileChangeHandler = (e) => {
        const name = e.target.name;
        const file = e.target.files[0];

        setArtist((prevState) => ({
            ...prevState,
            [name]: file,
        }));
    };

    const submitFormHandler = e => {
        e.preventDefault();

        const formData = new FormData();

        Object.keys(artist).forEach((key) => {
            formData.append(key, artist[key]);
        });

        dispatch(postAlbum(formData));
    };

    return (
        <>
            <Typography variant={'h4'} style={{marginBottom: '20px'}}>
                Add new album
            </Typography>
            <Grid container spacing={4} direction='column' component={'form'} onSubmit={submitFormHandler}>
                <Grid item xs>
                    <FormElement
                        required
                        type='text'
                        label='Album name'
                        onChange={inputChangeHandler}
                        name={'name'}
                        value={artist.name}
                    />
                </Grid>

                <Grid item xs>
                    <TextField
                        required
                        fullWidth
                        select
                        variant={'outlined'}
                        label={'Artists'}
                        name={'artist'}
                        value={artist.artist}
                        onChange={inputChangeHandler}
                    >
                        <MenuItem disabled>
                            <p>Select artist</p>
                        </MenuItem>
                        {artists.map((artist) => (
                            <MenuItem key={artist._id} value={artist._id}>
                                {artist.name}
                            </MenuItem>
                        ))}
                    </TextField>
                </Grid>
                <Grid item>
                    <FormElement
                        type='text'
                        required
                        label='Year'
                        onChange={inputChangeHandler}
                        name='year'
                        value={artist.year}
                    />
                </Grid>
                <FileInput onChange={fileChangeHandler} name='image' value={artist.image}/>

                <Grid item>
                    <Button variant="contained" color="primary" type="submit">
                        Create album
                    </Button>
                </Grid>
            </Grid>
        </>
    );
};

export default AddAlbum;