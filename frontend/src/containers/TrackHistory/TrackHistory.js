import {Card, CardHeader, Container, Grid, Typography} from "@material-ui/core";
import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {fetchTrackHistory} from "../../store/actions/TrackHistoryActions";

const TrackHistory = () => {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchTrackHistory());
    }, [dispatch]);

    const trackHistory = useSelector((state) => state.trackHistory.trackHistory);

    return (
        <Container>
            {trackHistory.length > 0 ? (
                <Grid container direction="column" spacing={2}>
                    {trackHistory.map((history) => (
                        <Card key={history._id} style={{margin: "10px", padding: "10px"}}>
                            <CardHeader title={"Имя Исполнителя " + history.track.album.artist.name}/>
                            <p>Название Трека {history.track.name}</p>
                            <p>Время прослушивание {history.datetime}</p>
                        </Card>
                    ))}
                </Grid>
            ) : (
                <Typography variant="h3"> Список Историй Пуст</Typography>
            )}
        </Container>
    );
};

export default TrackHistory;
