import React, {useEffect} from 'react';
import {Grid, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {fetchAlbums} from "../store/actions/AlbumAction";
import Albums from "../components/Albums";
import BackDrop from "../components/UI/BackDrop/BackDrop";

const AlbumList = ({location}) => {
    const dispatch = useDispatch();
    const albums = useSelector(state => state.albums.albums);
    const loading = useSelector(state => state.albums.loading);
    const user = useSelector(state => state.users.user);

    useEffect(() => {
        dispatch(fetchAlbums(location.search));
    }, [dispatch, location.search]);

    return (
        <Grid container direction={'column'} spacing={2}>
            <BackDrop
                loading={loading}
            />
            <Grid item container justifyContent={'space-between'} alignItems={'center'}>
                <Grid item>
                    {albums.length > 0  ? (
                        <Typography variant="h4">{albums[0].artist.name}</Typography>
                    ) : (
                        <Typography>Список Альбомов Пуст</Typography>
                    )}
                </Grid>
            </Grid>
            <Grid item container spacing={3}>
                {albums.map(album => (
                    album?.published === true || user?.role === 'admin' ?
                        <Albums
                            key={album._id}
                            title={album.name}
                            image={album.image}
                            date={album.year}
                            _id={album._id}
                            album={album}
                            location={location.search}
                        /> : null
                ))}
            </Grid>
        </Grid>
    );
};

export default AlbumList;