import React, {useEffect, useState} from 'react';
import {Button, Grid, MenuItem, TextField, Typography} from "@material-ui/core";
import FormElement from "../../components/UI/Form/FormElement";
import {useDispatch, useSelector} from "react-redux";
import {fetchAlbums} from "../../store/actions/AlbumAction";
import {number} from "prop-types";
import {postTrack} from "../../store/actions/TrackAction";

const AddAlbum = ({location}) => {
    const dispatch = useDispatch();

    const [artist, setArtist] = useState({
        name: '',
        album: '',
        duration: '',
        number: '',
    });

    useEffect(() => {
        dispatch(fetchAlbums(location.search));
    }, [dispatch]);

    const albums = useSelector(state => state.albums.albums);


    const inputChangeHandler = e => {
        const {name, value} = e.target;

        setArtist(prevState => ({
            ...prevState,
            [name]: value
        }));
    };


    const submitFormHandler = e => {
        e.preventDefault();
        dispatch(postTrack(artist));
    };

    return (
        <>
            <Typography variant={'h4'} style={{marginBottom: '20px'}}>
                Add new track
            </Typography>
            <Grid container spacing={4} direction='column' component={'form'} onSubmit={submitFormHandler}>
                <Grid item xs>
                    <FormElement
                        required
                        label='Track name'
                        onChange={inputChangeHandler}
                        name={'name'}
                        value={artist.name}
                    />
                </Grid>


                <Grid item xs>
                    <TextField
                        type='text'
                        required
                        fullWidth
                        select
                        variant={'outlined'}
                        label={'Albums'}
                        name={'album'}
                        value={artist.album}
                        onChange={inputChangeHandler}
                    >
                        <MenuItem disabled>
                            <p>Select artist</p>
                        </MenuItem>
                        {albums.map((album) => (
                            <MenuItem key={album._id} value={album._id}>
                                {album.name}
                            </MenuItem>
                        ))}
                    </TextField>
                </Grid>
                <Grid item>
                    <FormElement
                        type='text'
                        required
                        label='Duration'
                        onChange={inputChangeHandler}
                        name='duration'
                        value={artist.duration}
                    />
                </Grid>

                <Grid item xs>
                    <FormElement
                        type={number}
                        required
                        label='Number'
                        onChange={inputChangeHandler}
                        name='number'
                        value={artist.number}
                    />
                </Grid>

                <Grid item>
                    <Button variant="contained" color="primary" type="submit">
                        Create track
                    </Button>
                </Grid>
            </Grid>
        </>
    );
};

export default AddAlbum;