import React, {useState} from 'react';
import {Grid, makeStyles, Typography} from "@material-ui/core";
import FormElement from "../../components/UI/Form/FormElement";
import FileInput from "../../components/UI/Form/FileInput/FileInput";
import {useDispatch, useSelector} from "react-redux";
import {postArtist} from "../../store/actions/ArtistAction";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";

const useStyles = makeStyles(theme => ({
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

const AddArtist = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const [artist, setArtist] = useState({
        name: '',
        image: '',
        description: ''
    });

    const error = useSelector(state => state.artists.artistsError);
    const loading = useSelector(state => state.artists.createArtistLoading);

    const inputChangeHandler = e => {
        const {name, value} = e.target;

        setArtist(prevState => ({
            ...prevState,
            [name]: value
        }));
    };


    const fileChangeHandler = (e) => {
        const name = e.target.name;
        const file = e.target.files[0];

        setArtist((prevState) => ({
            ...prevState,
            [name]: file,
        }));
    };

    const submitFormHandler = e => {
        e.preventDefault();

        const formData = new FormData();

        Object.keys(artist).forEach((key) => {
            formData.append(key, artist[key]);
        });

        dispatch(postArtist(formData));
    };

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    return (
        <>
            <Typography variant={'h4'} style={{marginBottom: '20px'}}>
                Add new artist
            </Typography>
            <Grid
                container
                spacing={4}
                direction={'column'}
                component='form'
                onSubmit={submitFormHandler}
                noValidate
            >
                <Grid item xs>
                    <FormElement
                        type={'text'}
                        label={'Artist name'}
                        onChange={inputChangeHandler}
                        name={'name'}
                        value={artist.name}
                        required
                        error={getFieldError('name')}
                    />
                </Grid>

                <Grid item xs>
                    <FormElement
                        type={'text'}
                        label={'Description'}
                        onChange={inputChangeHandler}
                        name={'description'}
                        value={artist.description}
                    />
                </Grid>

                <FileInput
                    name={'image'}
                    label={'Image'}
                    onChange={fileChangeHandler}
                />
                <Grid item xs>
                    <ButtonWithProgress
                        type='submit'
                        fullWidth
                        variant='contained'
                        color='primary'
                        className={classes.submit}
                        loading={loading}
                        disabled={loading}
                    >
                        Create
                    </ButtonWithProgress>
                </Grid>
            </Grid>
        </>
    );
};

export default AddArtist;