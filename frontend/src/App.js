import './App.css';
import {Route, Switch} from 'react-router-dom'
import ArtistList from "./containers/ArtistList";
import AlbumList from "./containers/AlbumList";
import Tracks from "./containers/Tracks";
import Layout from "./components/UI/Layout/Layout";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import TrackHistory from "./containers/TrackHistory/TrackHistory";
import AddArtist from "./containers/AddArtist/AddArtist";
import AddAlbum from "./containers/AddAlbum/AddAlbum";
import AddTrack from "./containers/AddTrack/AddTrack";

const App = () => (
    <div className="App">
        <Layout>
            <Switch>
                <Route path='/' exact component={ArtistList}/>
                <Route path='/albums' component={AlbumList}/>
                <Route path='/tracks' component={Tracks}/>
                <Route path='/register' component={Register}/>
                <Route path={'/login'} component={Login}/>
                <Route path={'/track_history'} component={TrackHistory}/>
                <Route path={'/add_artist'} component={AddArtist}/>
                <Route path={'/add_album'} component={AddAlbum}/>
                <Route path={'/add_track'} component={AddTrack}/>
            </Switch>
        </Layout>
    </div>
);

export default App;
