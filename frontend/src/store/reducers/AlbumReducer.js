import {
    DELETE_ALBUM_FAILURE,
    DELETE_ALBUM_REQUEST, DELETE_ALBUM_SUCCESS,
    FETCH_ALBUM_FAILURE,
    FETCH_ALBUM_REQUEST,
    FETCH_ALBUM_SUCCESS, POST_ALBUM_FAILURE,
    POST_ALBUM_REQUEST, POST_ALBUM_SUCCESS, PUBLISH_ALBUM_FAILURE, PUBLISH_ALBUM_REQUEST, PUBLISH_ALBUM_SUCCESS
} from "../actions/AlbumAction";

const initialState = {
    albums: [],
    loading: false,
    btn: false
};

const albumReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ALBUM_REQUEST:
            return {...state, loading: true}
        case FETCH_ALBUM_SUCCESS:
            return {...state, albums: action.payload, loading: false}
        case FETCH_ALBUM_FAILURE:
            return {...state}
        case POST_ALBUM_REQUEST:
            return {...state, loading: true}
        case POST_ALBUM_SUCCESS:
            return {...state, loading: false}
        case POST_ALBUM_FAILURE:
            return {...state}
        case PUBLISH_ALBUM_REQUEST:
            return {...state, loading: true, btn: true}
        case PUBLISH_ALBUM_SUCCESS:
            return {...state, loading: false}
        case PUBLISH_ALBUM_FAILURE:
            return {...state}
        case DELETE_ALBUM_REQUEST:
            return {...state, loading: true}
        case DELETE_ALBUM_SUCCESS:
            return {...state, loading: false}
        case DELETE_ALBUM_FAILURE:
            return {...state}
        default:
            return state
    }
};

export default albumReducer;