import {
    DELETE_ARTIST_FAILURE,
    DELETE_ARTIST_REQUEST, DELETE_ARTIST_SUCCESS,
    FETCH_ARTIST_FAILURE,
    FETCH_ARTIST_REQUEST,
    FETCH_ARTIST_SUCCESS, POST_ARTIST_FAILURE,
    POST_ARTIST_REQUEST, POST_ARTIST_SUCCESS
} from "../actions/ArtistAction";

export const initialState = {
    artists: [],
    loading: false,
    artistsError: null,
    createArtistLoading: false
};

const ArtistReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ARTIST_REQUEST:
            return {...state, loading: true}
        case FETCH_ARTIST_SUCCESS:
            return {...state, artists: action.payload, loading: false}
        case FETCH_ARTIST_FAILURE:
            return {...state}
        case POST_ARTIST_REQUEST:
            return {...state, loading: true}
        case POST_ARTIST_SUCCESS:
            return {...state, artistsError: null, loading: false}
        case POST_ARTIST_FAILURE:
            return {...state, loading: false, artistsError: action.payload}
        case DELETE_ARTIST_REQUEST:
            return {...state, loading: true}
        case DELETE_ARTIST_SUCCESS:
            return {...state, loading: false}
        case DELETE_ARTIST_FAILURE:
            return {...state}
        default:
            return state;
    }
};

export default ArtistReducer;