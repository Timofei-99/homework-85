import {
    FETCH_TRACK_HISTORY_FAILURE,
    FETCH_TRACK_HISTORY_REQUEST,
    FETCH_TRACK_HISTORY_SUCCESS,
    POST_TRACK_HISTORY_FAILURE,
    POST_TRACK_HISTORY_REQUEST,
    POST_TRACK_HISTORY_SUCCESS,
} from "../actions/TrackHistoryActions";

const initialState = {
    errorTrackHistory: false,
    loadingTrackHistory: false,
    trackHistory: [],
};

const trackHistoryReducer = (state = initialState, action) => {
    switch (action.type) {
        case POST_TRACK_HISTORY_REQUEST:
            return {...state, loadingTrackHistory: true};
        case POST_TRACK_HISTORY_SUCCESS:
            return {...state, loadingTrackHistory: false};
        case POST_TRACK_HISTORY_FAILURE:
            return {...state, loadingTrackHistory: false, errorTrackHistory: action.error};
        case FETCH_TRACK_HISTORY_REQUEST:
            return {...state, loadingTrackHistory: true};
        case FETCH_TRACK_HISTORY_SUCCESS:
            return {...state, loadingTrackHistory: false, trackHistory: action.payload};
        case FETCH_TRACK_HISTORY_FAILURE:
            return {...state, loadingTrackHistory: false, errorTrackHistory: action.error};
        default:
            return state;
    }
};

export default trackHistoryReducer;
