import {
    DELETE_TRACK_FAILURE,
    DELETE_TRACK_REQUEST, DELETE_TRACK_SUCCESS,
    FETCH_TRACK_FAILURE,
    FETCH_TRACK_REQUEST,
    FETCH_TRACK_SUCCESS, PUBLISH_TRACK_FAILURE,
    PUBLISH_TRACK_REQUEST, PUBLISH_TRACK_SUCCESS
} from "../actions/TrackAction";

const initialState = {
    tracks: [],
    loading: false
};

const TracksReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_TRACK_REQUEST:
            return {...state, loading: true}
        case FETCH_TRACK_SUCCESS:
            return {...state, tracks: action.payload, loading: false}
        case FETCH_TRACK_FAILURE:
            return {...state}
        case PUBLISH_TRACK_REQUEST:
            return {...state, loading: true}
        case PUBLISH_TRACK_SUCCESS:
            return {...state, loading: false}
        case PUBLISH_TRACK_FAILURE:
            return {...state}
        case DELETE_TRACK_REQUEST:
            return {...state, loading: true}
        case DELETE_TRACK_SUCCESS:
            return {...state, loading: false}
        case DELETE_TRACK_FAILURE:
            return {...state}
        default:
            return state
    }
};

export default TracksReducer;