import {
    LOGIN_USER_FAILURE,
    LOGIN_USER_REQUEST,
    LOGIN_USER_SUCCESS, LOGOUT_USER_FAILURE, LOGOUT_USER_REQUEST, LOGOUT_USER_SUCCESS,
    REGISTER_USER_FAILURE,
    REGISTER_USER_SUCCESS
} from "../actions/userActions";

const initialState = {
    user: null,
    registerError: null,
    loginError: null,
    LoginLoading: false
};

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case REGISTER_USER_SUCCESS:
            return {...state, user: action.payload, registerError: null}
        case REGISTER_USER_FAILURE:
            return {...state, registerError: action.payload}
        case LOGIN_USER_REQUEST:
            return {...state, LoginLoading: true}
        case LOGIN_USER_SUCCESS:
            return {...state, user: action.payload, loginError: null, LoginLoading: false}
        case LOGIN_USER_FAILURE:
            return {...state, loginError: action.payload, LoginLoading: false}
        case LOGOUT_USER_REQUEST:
            return {...state}
        case LOGOUT_USER_SUCCESS:
            return {...state, user: null}
        case LOGOUT_USER_FAILURE:
            return {...state}
        default:
            return state;
    }
};

export default userReducer;