import {FETCH_ARTIST_FAILURE} from "./ArtistAction";
import axios from "axios";
import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {historyPush} from "./historyActions";

export const FETCH_TRACK_REQUEST = 'FETCH_TRACK_REQUEST';
export const FETCH_TRACK_SUCCESS = 'FETCH_TRACK_SUCCESS';
export const FETCH_TRACK_FAILURE = 'FETCH_TRACK_FAILURE';

export const POST_TRACK_REQUEST = 'POST_TRACK_REQUEST';
export const POST_TRACK_SUCCESS = 'POST_TRACK_SUCCESS';
export const POST_TRACK_FAILURE = 'POST_TRACK_FAILURE';

export const PUBLISH_TRACK_REQUEST = 'PUBLISH_TRACK_REQUEST';
export const PUBLISH_TRACK_SUCCESS = 'PUBLISH_TRACK_SUCCESS';
export const PUBLISH_TRACK_FAILURE = 'PUBLISH_TRACK_FAILURE';

export const DELETE_TRACK_REQUEST = 'DELETE_TRACK_REQUEST';
export const DELETE_TRACK_SUCCESS = 'DELETE_TRACK_SUCCESS';
export const DELETE_TRACK_FAILURE = 'DELETE_TRACK_FAILURE';



export const fetchTrackRequest = () => ({type: FETCH_TRACK_REQUEST});
export const fetchTrackSuccess = (track) => ({type: FETCH_TRACK_SUCCESS, payload: track});
export const fetchTrackFailure = () => ({type: FETCH_ARTIST_FAILURE});

export const postTrackRequest = () => ({type: POST_TRACK_REQUEST});
export const postTrackSuccess = () => ({type: POST_TRACK_SUCCESS});
export const postTrackFailure = () => ({type: POST_TRACK_FAILURE});

export const publishTrackRequest = () => ({type: PUBLISH_TRACK_REQUEST});
export const publishTrackSuccess = () => ({type: PUBLISH_TRACK_SUCCESS});
export const publishTrackFailure = () => ({type: PUBLISH_TRACK_FAILURE});

export const deleteTrackRequest = () => ({type: DELETE_TRACK_REQUEST});
export const deleteTrackSuccess = () => ({type: DELETE_TRACK_SUCCESS});
export const deleteTrackFailure = () => ({type: DELETE_TRACK_FAILURE});

export const fetchTracks = (_id) => {
    return async (dispatch) => {
        let url = '/tracks';

        if (url) {
            url += _id
        }
        try {
            dispatch(fetchTrackRequest());
            const {data} = await axiosApi.get(url)
            console.log(data)
            dispatch(fetchTrackSuccess(data));
        } catch (e) {
            dispatch(fetchTrackFailure(e))
        }
    }
};

export const postTrack = (trackData) => {
    return async (dispatch) => {
        try {
            dispatch(postTrackRequest());
            await axiosApi.post('/tracks', trackData);
            dispatch(postTrackSuccess());
            toast.success('Track has been created');
            dispatch(historyPush('/'));
        } catch (e) {
            dispatch(postTrackFailure(e));
            toast.error('Error!');
        }
    }
};

export const deleteTrack = (id, location) => {
    return async (dispatch, getState) => {
        const token = getState().users.user.token;
        if (token === null) {
            toast.warning('You are not logged in');
        }
        try {
            dispatch(deleteTrackRequest());
            await axiosApi.delete(`/tracks/${id}`)
            dispatch(deleteTrackSuccess());
            dispatch(fetchTracks(location));
            toast.success('Track was deleted');
        } catch (e) {
            dispatch(deleteTrackFailure(e));
        }
    }
}

export const publishTrack = (id) => {
    return async (dispatch, getState) => {
        const token = getState().users.user.token;
        if (token === null) {
            toast.warning("You are not logged in");
        }
        try {
            dispatch(publishTrackRequest());
            await axiosApi.post(`/tracks/${id}/publish`);
            dispatch(publishTrackSuccess());
        } catch (e) {
            dispatch(publishTrackFailure(e));
        }
    }
};
