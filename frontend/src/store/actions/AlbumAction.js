import axiosApi from "../../axiosApi";
import {historyPush} from "./historyActions";
import {toast} from "react-toastify";
import {PUBLISH_ARTIST_FAILURE} from "./ArtistAction";

export const FETCH_ALBUM_REQUEST = 'FETCH_ALBUM_REQUEST';
export const FETCH_ALBUM_SUCCESS = 'FETCH_ALBUM_SUCCESS';
export const FETCH_ALBUM_FAILURE = 'FETCH_ALBUM_FAILURE';

export const POST_ALBUM_REQUEST = 'POST_ALBUM_REQUEST';
export const POST_ALBUM_SUCCESS = 'POST_ALBUM_SUCCESS';
export const POST_ALBUM_FAILURE = 'POST_ALBUM_FAILURE';

export const PUBLISH_ALBUM_REQUEST = 'PUBLISH_ALBUM_REQUEST';
export const PUBLISH_ALBUM_SUCCESS = 'PUBLISH_ALBUM_SUCCESS';
export const PUBLISH_ALBUM_FAILURE = 'PUBLISH_ALBUM_FAILURE';

export const DELETE_ALBUM_REQUEST = 'DELETE_ALBUM_REQUEST';
export const DELETE_ALBUM_SUCCESS = 'DELETE_ALBUM_SUCCESS';
export const DELETE_ALBUM_FAILURE = 'DELETE_ALBUM_FAILURE';

export const fetchAlbumRequest = () => ({type: FETCH_ALBUM_REQUEST});
export const fetchAlbumSuccess = album => ({type: FETCH_ALBUM_SUCCESS, payload: album});
export const fetchAlbumFailure = () => ({type: FETCH_ALBUM_FAILURE});

export const postAlbumRequest = () => ({type: POST_ALBUM_REQUEST});
export const postAlbumSuccess = () => ({type: POST_ALBUM_SUCCESS});
export const postAlbumFailure = () => ({type: POST_ALBUM_FAILURE});

export const publishAlbumRequest = () => ({type: PUBLISH_ALBUM_REQUEST});
export const publishAlbumSuccess = () => ({type: PUBLISH_ALBUM_SUCCESS});
export const publishAlbumFailure = () => ({type: PUBLISH_ARTIST_FAILURE});

export const deleteAlbumRequest = () => ({type: DELETE_ALBUM_REQUEST});
export const deleteAlbumSuccess = () => ({type: DELETE_ALBUM_SUCCESS});
export const deleteAlbumFailure = () => ({type: DELETE_ALBUM_FAILURE});

export const fetchAlbums = (_id) => {
    return async (dispatch) => {
        let url = '/albums';

        if (url) {
            url += _id
        }

        try {
            dispatch(fetchAlbumRequest());
            const {data} = await axiosApi.get(url);
            dispatch(fetchAlbumSuccess(data));
        } catch (e) {
            dispatch(fetchAlbumFailure(e));
        }
    }
};

export const postAlbum = (albumData) => {
    return async (dispatch, getState) => {
        const token = getState().users.user.token;

        if (token === null) {
            dispatch(historyPush('/login'));
            toast.warning("You should login!");
        }

        try {
            dispatch(postAlbumRequest());
            await axiosApi.post('/albums', albumData);
            dispatch(postAlbumSuccess());
            dispatch(historyPush('/'));
            toast.success("Album has been added!");
        } catch (e) {
            if (e.response && e.response.data) {
                dispatch(postAlbumFailure(e.response.data));
            } else {
                dispatch(postAlbumFailure({global: "No internet!"}));
            }
        }
    }
};

export const publishAlbum = (id) => {
    return async (dispatch, getState) => {
        const token = getState().users.user.token;
        if (token === null) {
            toast.warning("You are not logged in");
        }
        try {
            dispatch(publishAlbumRequest());
            await axiosApi.post(`/albums/${id}/publish`);
            dispatch(publishAlbumSuccess());
        } catch (e) {
            dispatch(publishAlbumFailure(e));
        }
    };
};

export const deleteAlbum = (id, location) => {
    return async (dispatch, getState) => {
        const token = getState().users.user.token;
        if (token === null) {
            toast.warning('You are not logged in');
        }
        try {
            dispatch(deleteAlbumRequest());
            await axiosApi.delete(`/albums/${id}`);
            dispatch(deleteAlbumSuccess())
            dispatch(fetchAlbums(location))
            toast.success('Album was deleted');
        } catch (e) {
            dispatch(deleteAlbumFailure(e));
        }
    }
}
