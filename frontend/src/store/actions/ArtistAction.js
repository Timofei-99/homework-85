import {toast} from "react-toastify";
import axiosApi from "../../axiosApi";
import {historyPush} from "./historyActions";

export const FETCH_ARTIST_REQUEST = "FETCH_ARTIST_REQUEST";
export const FETCH_ARTIST_SUCCESS = "FETCH_ARTIST_SUCCESS";
export const FETCH_ARTIST_FAILURE = "FETCH_ARTIST_FAILURE";

export const POST_ARTIST_REQUEST = "POST_ARTIST_REQUEST";
export const POST_ARTIST_SUCCESS = "POST_ARTIST_SUCCESS";
export const POST_ARTIST_FAILURE = "POST_ARTIST_FAILURE";

export const PUBLISH_ARTIST_REQUEST = 'PUBLISH_ARTIST_REQUEST';
export const PUBLISH_ARTIST_SUCCESS = 'PUBLISH_ARTIST_SUCCESS';
export const PUBLISH_ARTIST_FAILURE = 'PUBLISH_ARTIST_FAILURE';

export const DELETE_ARTIST_REQUEST = 'DELETE_ARTIST_REQUEST';
export const DELETE_ARTIST_SUCCESS = 'DELETE_ARTIST_SUCCESS';
export const DELETE_ARTIST_FAILURE = 'DELETE_ARTIST_FAILURE';

export const fetchArtistRequest = () => ({type: FETCH_ARTIST_REQUEST});
export const fetchArtistSuccess = artist => ({type: FETCH_ARTIST_SUCCESS, payload: artist});
export const fetchArtistFailure = error => ({type: FETCH_ARTIST_FAILURE});

export const postArtistRequest = () => ({type: POST_ARTIST_REQUEST});
export const postArtistSuccess = () => ({type: POST_ARTIST_SUCCESS});
export const postArtistFailure = error => ({type: POST_ARTIST_FAILURE, payload: error});

export const publishArtistRequest = () => ({type: PUBLISH_ARTIST_REQUEST});
export const publishArtistSuccess = () => ({type: PUBLISH_ARTIST_SUCCESS});
export const publishArtistFailure = () => ({type: PUBLISH_ARTIST_FAILURE});

export const deleteArtistRequest = () => ({type: DELETE_ARTIST_REQUEST});
export const deleteArtistSuccess = () => ({type: DELETE_ARTIST_SUCCESS});
export const deleteArtistFailure = () => ({type: DELETE_ARTIST_FAILURE});

export const fetchArtist = () => {
    return async (dispatch) => {
        try {
            dispatch(fetchArtistRequest());
            const {data} = await axiosApi.get('/artists');
            dispatch(fetchArtistSuccess(data));
        } catch (e) {
            dispatch(fetchArtistFailure(e));
            toast.error('Could not fetch artists', {
                theme: "colored"
            });
        }
    }
};

export const postArtist = (artistData) => {
    return async (dispatch, getState) => {
        const token = getState().users.user.token;

        if (token === null) {
            dispatch(historyPush('/login'));
            toast.warning("You should login!");
        }

        try {
            dispatch(postArtistRequest());
            await axiosApi.post('/artists', artistData);
            dispatch(postArtistSuccess());
            dispatch(historyPush('/'));
            toast.success("Artist has been added!");
        } catch (e) {
            toast.error('Could not add artist');
            if (e.response && e.response.data) {
                dispatch(postArtistFailure(e.response.data));
            } else {
                dispatch(postArtistFailure({global: "No internet!"}));
            }
        }
    }
};


export const toPublishArtist = (id) => {
    return async (dispatch, getState) => {
        const token = getState().users.user.token;
        if (token === null) {
            toast.warning("You are not logged in");
        }
        try {
            await axiosApi.post(`/artists/${id}/publish`);
            dispatch(publishArtistSuccess());
        } catch (e) {
            dispatch(publishArtistFailure(e));
        }
    };
};

export const deleteArtist = (id) => {
    return async (dispatch, getState) => {
        const token = getState().users.user.token;
        if (token === null) {
            toast.warning("You are not logged in");
        }
        try {
            dispatch(deleteArtistRequest());
            await axiosApi.delete(`/artists/${id}`)
            dispatch(deleteArtistSuccess());
            dispatch(fetchArtist())
            toast.success('Artist was deleted');
        } catch (e) {
            dispatch(deleteArtistFailure(e));
        }
    }
}