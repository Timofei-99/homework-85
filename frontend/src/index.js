import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {Provider} from "react-redux";
import {Router} from "react-router-dom";
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import {createTheme, MuiThemeProvider} from "@material-ui/core";
import history from "./history";
import store from "./store/configureStore";


const theme = createTheme({
    props: {
        MuiTextField: {
            variant: 'outlined',
            fullWidth: true
        },
    },
})


const app = (
    <Provider store={store}>
        <Router history={history}>
            <MuiThemeProvider theme={theme}>
                <App/>
                <ToastContainer/>
            </MuiThemeProvider>
        </Router>
    </Provider>
);


ReactDOM.render(app, document.getElementById('root'));


