import React from 'react';
import {Button, Card, CardHeader, CardMedia, Grid, makeStyles} from "@material-ui/core";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {deleteAlbum, publishAlbum} from "../store/actions/AlbumAction";

const useStyles = makeStyles({
    card: {
        height: "20%",
        backgroundColor: "#e8e8e8",
        border: "1px solid black",
        padding: "5px",
    },
    media: {
        height: 0,
        paddingTop: "70.25%",
        borderBottom: "1px solid black",
    },
    link: {
        textDecoration: "none",
        textAlign: "center",
    },
});

const Albums = ({_id, title, date, image, album, location}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);

    let cardImage;

    if (image) {
        cardImage = 'http://localhost:8000/uploads/' + image;
    }


    return (
        <Grid item xs>
            <Link to={`/tracks?album=${_id}`}>
                <Grid>
                    <Card className={classes.card}>
                        {image ? <CardMedia image={cardImage} className={classes.media}/> : null
                        }
                        <CardHeader title={title}/>
                        <p> Год Выпуска: {date}</p>
                    </Card>
                </Grid>
            </Link>
            <Grid item>
                {user && user.role === 'admin' && (
                    <Button onClick={() => dispatch(deleteAlbum(_id, location))} variant='outlined' style={{marginTop: '10px'}}>
                        Delete
                    </Button>
                )}
            </Grid>
            <Grid item>
                {user && user.role === 'admin' && (
                    <Button onClick={() => dispatch(publishAlbum(_id))} variant='outlined'
                            style={{marginTop: '10px'}}>
                        {album.published === false ? 'Publish' : 'Unpublish'}
                    </Button>
                )}
            </Grid>
        </Grid>
    );
};

export default Albums;