import React, {useState} from 'react';
import Button from "@material-ui/core/Button";
import {Avatar, Menu, MenuItem} from "@material-ui/core";
import {Link} from "react-router-dom";
import {useDispatch} from "react-redux";
import {logoutUser} from "../../../../store/actions/userActions";
import {APIURL} from "../../../../config";

const UserMenu = ({user}) => {
    const dispatch = useDispatch();
    const [anchorEl, setAnchorEl] = useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    let cardImage;

    if (user.avatarImage) {
        if (user.avatarImage.indexOf("fixtures") === 0 || user.avatarImage.indexOf("uploads") === 0) {
            cardImage = APIURL + "/" + user.avatarImage;
        } else {
            cardImage = user.avatarImage;
        }
    }

    return (
        <>
            <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick} color={'inherit'}>
                Hello, {user.displayName}!
                <Avatar alt={user.displayName} src={cardImage} style={{marginLeft: '10px'}}/>
            </Button>
            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <MenuItem component={Link} to={'/add_artist'}>Add artist</MenuItem>
                <MenuItem component={Link} to={'/add_album'}>Add album</MenuItem>
                <MenuItem component={Link} to={'/add_track'}>Add track</MenuItem>
                <MenuItem onClick={() => dispatch(logoutUser())}>Logout</MenuItem>
                <MenuItem component={Link} to={'/track_history'}>Track History</MenuItem>
            </Menu>
        </>
    );
};

export default UserMenu;