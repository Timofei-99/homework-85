import React from 'react';
import FacebookLoginButton from 'react-facebook-login/dist/facebook-login-render-props';
import {Button} from "@material-ui/core";
import FacebookIcon from '@material-ui/icons/Facebook';
import {useDispatch} from "react-redux";
import {facebookLogin} from "../../../store/actions/userActions";
import {facebookId} from "../../../config";

const FacebookLogin = () => {
    const dispatch = useDispatch();

    const facebookResponse = response => {
        dispatch(facebookLogin(response))
    };


    return (
        <FacebookLoginButton
            appId={facebookId}
            fields='name,email,picture'
            render={props => (
                <Button
                    fullWidth
                    variant='outlined'
                    color='primary'
                    onClick={props.onClick}
                    startIcon={<FacebookIcon/>}
                >
                    Login with facebook
                </Button>
            )}
            callback={facebookResponse}
        />


    );
};

export default FacebookLogin;