import {Button, Card, CardHeader, Grid, makeStyles} from "@material-ui/core";
import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {postTrackHistory} from "../store/actions/TrackHistoryActions";
import {deleteTrack, publishTrack} from "../store/actions/TrackAction";

const useStyles = makeStyles(theme => ({
    card: {
        height: "100%",
        border: "2px solid #367ce3",
        padding: '10px',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    media: {
        height: 0,
        paddingTop: "90.25%",
    },
    link: {
        textDecoration: "none",
        textAlign: "center",
    },
}))

const Track = ({_id, track, artist, album, duration, number, publish, location}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);



    const add = () => {
        dispatch(postTrackHistory(_id))
    };

    return (
        <Grid item xs onClick={add}>
            <Grid item className={classes.link}>
                <Card className={classes.card}>
                    <CardHeader title={track}/>
                    <p>{album}</p>
                    <p>{duration}</p>
                    <p>{number}</p>
                    <Button
                        style={{marginTop: '10px'}}
                        variant={'outlined'}
                        onClick={() => dispatch(publishTrack(_id))}
                    >
                        {publish.published === false ? 'Publish' : 'Unpublish'}
                    </Button>
                    {user && user.role === 'admin' && (
                        <Button onClick={() => dispatch(deleteTrack(_id, location))} variant='outlined'
                                style={{marginTop: '10px'}}>
                            Delete
                        </Button>
                    )}
                </Card>
            </Grid>
        </Grid>
    );
};

export default Track;