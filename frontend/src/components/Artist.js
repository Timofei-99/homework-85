import React from 'react';
import {Button, Card, CardHeader, CardMedia, Grid, makeStyles} from "@material-ui/core";
import {Link} from "react-router-dom";
import MusicImage from '../assets/image/pngtree-headphones-icon-png-image_1770475.jpeg';
import {useDispatch, useSelector} from "react-redux";
import {deleteArtist, toPublishArtist} from "../store/actions/ArtistAction";

const useStyles = makeStyles(theme => ({
    card: {
        height: "100%",
        border: "2px solid #367ce3",
        padding: '10px'
    },
    media: {
        height: '100px',
        paddingTop: "90.25%",
    },
    link: {
        textDecoration: "none",
        textAlign: "center",
        textDecorationLine: 'none'
    },
}))

const Artist = ({_id, title, image, artist}) => {
    const dispatch = useDispatch();
    const classes = useStyles();
    const user = useSelector(state => state.users.user);


    let cardImage = MusicImage;

    if (image) {
        cardImage = 'http://localhost:8000/uploads/' + image
    }
    return (
        <Grid item xs style={{border: '2px solid black'}} spacing={2}>
            <Grid item className={classes.link}>
                <Link to={`/albums?artist=${_id}`}>
                    <Card className={classes.card}>
                        <CardHeader title={title}/>
                        <CardMedia
                            className={classes.media}
                            image={cardImage}
                        />
                    </Card>
                </Link>
                <Grid item>
                    {user && user.role === 'admin' && (
                        <Button onClick={() => dispatch(deleteArtist(_id))} variant='outlined'
                                style={{marginTop: '10px'}}>
                            Delete
                        </Button>
                    )}
                </Grid>
                <Grid item>
                    {user && user.role === 'admin' && (
                        <Button onClick={() => dispatch(toPublishArtist(_id))} variant='outlined'
                                style={{marginTop: '10px'}}>
                            {artist.published === false ? 'Publish' : 'Unpublish'}
                        </Button>
                    )}
                </Grid>
            </Grid>
        </Grid>
    );
};

export default Artist;


//
//
