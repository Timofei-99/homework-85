const mongoose = require('mongoose');
const config = require('./config');
const Artist = require('./models/Artist');
const Album = require('./models/Album');
const Track = require('./models/Track');
const User = require('./models/User');
const {nanoid} = require('nanoid');


const run = async () => {
    await mongoose.connect(config.db.url, config.db.options);

    const collection = await mongoose.connection.db.listCollections().toArray();

    for (let coll of collection) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [artist, artist_2] = await Artist.create(
        {
            name: 'Maks Korj',
            image: '0342b6ecce05923c32337f609cbb8516.jpeg',
            description: 'Awesome Artist in the World!'
        },
        {
            name: 'Billie Eilish',
            image: 'BE-8.jpeg',
            description: 'Best young artist in USA'
        },
    );

    const [album, album2] = await Album.create(
        {
            name: 'Life Kaif',
            artist: artist,
            year: '2012',
            image: 'kaif.png'
        },
        {
            name: 'Happier Than Ever',
            artist: artist_2,
            year: '2021',
            image: 'happy.jpeg'
        },
    );

    await Track.create(
        {
            name: 'Wild life',
            album: album,
            duration: '3:03',
            number: '1'
        },
        {
            name: 'New track!',
            album: album,
            duration: '2:50',
            number: '2'
        },
        {
            name: 'Never give up',
            album: album,
            duration: '2:40',
            number: '3'
        },
        {
            name: 'Road to the moon!',
            album: album,
            duration: '4:50',
            number: '4'
        },
        {
            name: 'Time',
            album: album,
            duration: '3:00',
            number: '5'
        },
        {
            name: 'Army',
            album: album,
            duration: '3:32',
            number: '6'
        },
        {
            name: 'My love',
            album: album,
            duration: '2:30',
            number: '7'
        },
        {
            name: 'Emili',
            album: album,
            duration: '2:46',
            number: '8'
        },
        {
            name: 'Lovely',
            album: album2,
            duration: '2:46',
            number: '1'
        },
        {
            name: 'Bad boy',
            album: album2,
            duration: '2:30',
            number: '2'
        },
        {
            name: 'Lovely',
            album: album2,
            duration: '3:05',
            number: '3'
        },
        {
            name: 'LALALA',
            album: album2,
            duration: '3:40',
            number: '4'
        },
        {
            name: 'LOLOLO',
            album: album2,
            duration: '2:50',
            number: '5'
        },
    );

    await User.create(
        {
            email: 'tima9953@gmail.com',
            displayName: 'Admin',
            password: '123',
            token: nanoid(),
            role: 'admin'
        },
        {
            email: 'tim@gmail.com',
            displayName: 'Tima',
            password: '321',
            token: nanoid(),
            role: 'user'
        },
    );

    await mongoose.connection.close();
};

run().catch(console.error);
