const mongoose = require('mongoose');
const idvalidator = require('mongoose-id-validator');

const AlbumSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    artist: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Artist',
        required: true
    },
    year: {
        type: String,
        required: true,
    },
    published: {
        type: Boolean,
        required: true,
        default: false,
        enum: [true, false]
    },
    image: String
});

AlbumSchema.plugin(idvalidator);
const Album = mongoose.model('Album', AlbumSchema);
module.exports = Album;

