const mongoose = require('mongoose');

const ArtistSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    image: String,
    description: {
        type: String
    },
    published: {
        type: Boolean,
        required: true,
        default: false,
        enum: [true, false]
    },
});

const Artist = mongoose.model('Artist', ArtistSchema);

module.exports = Artist;